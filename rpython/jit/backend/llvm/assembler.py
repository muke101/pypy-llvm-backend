from rpython.jit.backend.model import CompiledLoopToken
from rpython.jit.backend.llsupport.assembler import BaseAssembler
from rpython.rtyper.lltypesystem import lltype
from rpython.jit.backend.llvm.llvm_api import CString
from rpython.jit.backend.llsupport import jitframe
from rpython.rtyper.lltypesystem.rffi import constcharp2str

class LLVMAssembler(BaseAssembler):
    def __init__(self, cpu, optimise=True):
        self.cpu = cpu
        self.llvm = cpu.llvm
        self.debug = cpu.debug
        self.optimise = optimise
        self.resource_trackers = {} #map modules to compiled binary
        self.llvm.InitializeNativeTarget(None)
        self.llvm.InitializeNativeAsmPrinter(None)
        self.initialise_jit()
        self.pass_manager1 = self.llvm.CreatePassManager(None)
        self.pass_manager = self.llvm.CreatePassManager(None)
        #self.add_O3_passes()
        self.add_cheap_opt_passes()

    def initialise_jit(self):
        jit_builder = self.llvm.CreateLLJITBuilder(None)
        if self.debug and jit_builder._cast_to_int() == 0:
            raise Exception("JIT Builder is Null")
        self.cpu_name = self.llvm.GetHostCPUName(None)
        self.cpu_features = self.llvm.GetHostCPUFeatures(None)
        self.triple = self.llvm.GetTargetTriple(None)
        target = self.llvm.GetTarget(self.triple)
        enums = lltype.malloc(self.llvm.JITEnums, flavor='raw')
        self.llvm.SetJITEnums(enums)
        opt_level = enums.codegenlevel
        reloc_mode = enums.reloc
        code_model = enums.codemodel
        self.target_machine = self.llvm.CreateTargetMachine(
            target, self.triple, self.cpu_name, self.cpu_features,
            opt_level, reloc_mode, code_model
        )
        lltype.free(enums, flavor='raw')
        self.data_layout = self.llvm.CreateTargetDataLayout(self.target_machine)
        jit_target_machine_builder = self.llvm.JITTargetMachineBuilderCreateFromTargetMachine(
                                            self.target_machine)
        self.llvm.LLJITBuilderSetJITTargetMachineBuilder(jit_builder,
                                                         jit_target_machine_builder)

        self.LLJIT = self.llvm.CreateLLJIT(jit_builder)
        if self.debug and self.LLJIT._cast_to_int() == 0:
            raise Exception("Failed To Create JIT")
        self.DyLib = self.llvm.LLJITGetMainJITDylib(self.LLJIT)
        if self.debug and self.DyLib._cast_to_int() == 0:
            raise Exception("DyLib is Null")

        self.llvm.AddDynamicLibrarySearchGenerator(self.LLJIT, self.DyLib)

    #hack to get around repeat definitions of a function all modules need
    def compile_malloc(self, module):
        module = self.llvm.CloneModule(module)
        ctx = self.cpu.thread_safe_context
        thread_safe_module = self.llvm.CreateThreadSafeModule(module, ctx)
        if self.debug and thread_safe_module._cast_to_int() == 0:
            raise Exception("TSM is Null")
        failure = self.llvm.LLJITAddModuleNoRT(self.LLJIT,
                                               self.DyLib,
                                               thread_safe_module)
        if self.debug and failure._cast_to_int():
            print(constcharp2str(self.llvm.GetErrorMessage(failure)))
            raise Exception("Failed To Add Module To JIT")

    def jit_compile(self, module, looptoken, inputargs, dispatcher,
                    name, is_bridge=False):

        if is_bridge:
            clt = looptoken.compiled_loop_token
            clt.compiling_a_bridge()
            #free the old compiled binary
            tracker = self.resource_trackers[module._cast_to_int()]
            failure = self.llvm.ResourceTrackerRemove(tracker)
            if self.debug and failure._cast_to_int():
                print(constcharp2str(self.llvm.GetErrorMessage(failure)))
                raise Exception("Failed to remove old resource tracker")
        else:
            clt = CompiledLoopToken(self.cpu, looptoken.number)
            looptoken.compiled_loop_token = clt
            clt._debug_nbargs = dispatcher.args_size/self.cpu.WORD
            locs = [self.cpu.WORD*i for i in range(len(inputargs))]
            clt._ll_initial_locs = locs
        self.resource_trackers[module._cast_to_int()] = tracker = self.llvm.CreateResourceTracker(self.DyLib)
        frame_info = lltype.malloc(jitframe.JITFRAMEINFO, flavor='raw')
        frame_info.jfi_frame_depth = looptoken.jitframe_depth
        frame_size = (dispatcher.args_size + self.cpu.WORD
        + dispatcher.local_vars_size) #args+ret addr+vars
        frame_info.jfi_frame_size = frame_size
        clt.frame_info = frame_info

        #LLVMs JIT takes ownership and deletes the module, we want to keep the original around for later modifications
        module = self.llvm.CloneModule(module)
        if not self.debug or self.optimise:
            #FIXME
            self.llvm.RunPassManager(self.pass_manager1, module)
            self.llvm.RunPassManager(self.pass_manager, module)
        if self.debug:
            self.cpu.write_ir(module, "opt")
        ctx = self.cpu.thread_safe_context
        thread_safe_module = self.llvm.CreateThreadSafeModule(module, ctx)
        if self.debug and thread_safe_module._cast_to_int() == 0:
            raise Exception("TSM is Null")
        failure = self.llvm.LLJITAddModule(self.LLJIT,
                                           tracker,
                                           thread_safe_module) #looking up a symbol in a module added to the LLVM Orc JIT invokes JIT compilation of the whole module
        if self.debug and failure._cast_to_int():
            print(constcharp2str(self.llvm.GetErrorMessage(failure)))
            raise Exception("Failed To Add Module To JIT")
        cstring = CString(name)
        addr = self.llvm.LLJITLookup(self.LLJIT,
                                     cstring.ptr)._cast_to_int()
        if self.debug and addr == 0:
            raise Exception("Trace Function is Null")
        looptoken._ll_function_addr = addr
        # import pdb
        # pdb.set_trace()
        # print("trace addr: ", addr)
        # self.llvm.create_breakpoint()

    def add_opt_passes(self): #TODO: add mem2reg pass!!
        # Analysis
        self.llvm.AddTargetAnalysisPasses(self.pass_manager1, self.target_machine)
        self.llvm.AddTargetLibraryInfoPass(self.pass_manager, self.triple)
        self.llvm.AddTypeBasedAliasAnalysisPass(self.pass_manager)
        self.llvm.AddScopedNoAliasAAPass(self.pass_manager)
        self.llvm.AddBasicAliasAnalysisPass(self.pass_manager)
        self.llvm.AddInferFunctionAttrsPass(self.pass_manager)

        # CFG simplification
        self.llvm.AddCFGSimplificationPass(self.pass_manager)
        self.llvm.AddScalarReplAggregatesPass(self.pass_manager)
        self.llvm.AddJumpThreadingPass(self.pass_manager)
        self.llvm.AddCorrelatedValuePropagationPass(self.pass_manager)

        # Redundancy elimination
        self.llvm.AddReassociatePass(self.pass_manager)
        self.llvm.AddEarlyCSEPass(self.pass_manager)
        self.llvm.AddGVNPass(self.pass_manager)
        self.llvm.AddInstructionSimplifyPass(self.pass_manager)
        self.llvm.AddInstructionCombiningPass(self.pass_manager)

        # Loop optimisations
        self.llvm.AddLoopSimplifyPass(self.pass_manager) # TODO: seperate out the two passes bundled together in this wrapper
        self.llvm.AddLICMPass(self.pass_manager)
        self.llvm.AddLoopRotatePass(self.pass_manager)
        self.llvm.AddLoopIdiomPass(self.pass_manager)
        self.llvm.AddIndVarSimplifyPass(self.pass_manager)
        self.llvm.AddLICMPass(self.pass_manager)
        self.llvm.AddLoopUnswitchPass(self.pass_manager)
        self.llvm.AddLoopDistributePass(self.pass_manager)
        self.llvm.AddLoopInterchangePass(self.pass_manager)
        self.llvm.AddLICMPass(self.pass_manager)
        self.llvm.AddLoopUnswitchPass(self.pass_manager)
        self.llvm.AddLoopStrengthReducePass(self.pass_manager)

        # Loop optimisation cleanup
        self.llvm.AddGVNPass(self.pass_manager)
        self.llvm.AddCFGSimplificationPass(self.pass_manager)
        self.llvm.AddSCCPPass(self.pass_manager)
        self.llvm.AddDCEPass(self.pass_manager)
        self.llvm.AddAggressiveInstCombinerPass(self.pass_manager)

        # Vectorisation
        self.llvm.AddLoopVectorizePass(self.pass_manager)
        self.llvm.AddSLPVectorizePass(self.pass_manager)

    def add_cheap_opt_passes(self):
        # CFG simplification
        self.llvm.AddCFGSimplificationPass(self.pass_manager)
        self.llvm.AddScalarReplAggregatesPass(self.pass_manager)
        self.llvm.AddJumpThreadingPass(self.pass_manager)
        self.llvm.AddCorrelatedValuePropagationPass(self.pass_manager)

        # Redundancy elimination
        self.llvm.AddReassociatePass(self.pass_manager)
        self.llvm.AddEarlyCSEPass(self.pass_manager)
        self.llvm.AddGVNPass(self.pass_manager)

        # Analysis
        self.llvm.AddTargetAnalysisPasses(self.pass_manager1, self.target_machine)
        self.llvm.AddTargetLibraryInfoPass(self.pass_manager, self.triple)
        self.llvm.AddTypeBasedAliasAnalysisPass(self.pass_manager)
        self.llvm.AddScopedNoAliasAAPass(self.pass_manager)
        self.llvm.AddBasicAliasAnalysisPass(self.pass_manager)

        # Loop optimisations
        self.llvm.AddLoopSimplifyPass(self.pass_manager) # TODO: seperate out the two passes bundled together in this wrapper
        self.llvm.AddLICMPass(self.pass_manager)
        self.llvm.AddLoopRotatePass(self.pass_manager)
        self.llvm.AddLoopIdiomPass(self.pass_manager)
        self.llvm.AddIndVarSimplifyPass(self.pass_manager)
        self.llvm.AddLICMPass(self.pass_manager)
        self.llvm.AddLoopDistributePass(self.pass_manager)
        self.llvm.AddLoopInterchangePass(self.pass_manager)

        # Loop optimisation cleanup
        self.llvm.AddCFGSimplificationPass(self.pass_manager)
        self.llvm.AddSCCPPass(self.pass_manager)
        self.llvm.AddDCEPass(self.pass_manager)
        self.llvm.AddInstructionCombiningPass(self.pass_manager)

        # Vectorisation
        self.llvm.AddLoopVectorizePass(self.pass_manager)
        self.llvm.AddSLPVectorizePass(self.pass_manager)

    def add_O3_passes(self):
        pass_manager_builder = self.llvm.PassManagerBuilderCreate(None)
        self.llvm.SetOptLevel(pass_manager_builder, 3)
        self.llvm.PopulatePassManager(pass_manager_builder, self.pass_manager)
        self.llvm.PassManagerBuilderDispose(pass_manager_builder)

    def __del__(self):
        self.llvm.DisposePassManager(self.pass_manager)
