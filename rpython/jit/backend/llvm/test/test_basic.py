import py
from rpython.jit.backend.test.runner_test import LLtypeBackendTest
from rpython.jit.backend.llvm.runner import LLVM_CPU
from rpython.jit.backend.detect_cpu import getcpuclass
from rpython.jit.metainterp.warmspot import ll_meta_interp
from rpython.jit.metainterp.test import support, test_ajit
from rpython.jit.codewriter.policy import StopAtXPolicy
from rpython.rlib.jit import JitDriver

class JitLLVMMixin(support.LLJitMixin):
    CPUClass = LLVM_CPU
    # we have to disable unroll
    enable_opts = "intbounds:rewrite:virtualize:string:earlyforce:pure:heap"
    basic = False

    def check_jumps(self, maxcount):
        pass

class Jit386Mixin(support.LLJitMixin):
    CPUClass = getcpuclass()
    # we have to disable unroll
    enable_opts = "intbounds:rewrite:virtualize:string:earlyforce:pure:heap"
    basic = False

    def check_jumps(self, maxcount):
        pass

class TestBasicLLVM(JitLLVMMixin, test_ajit.BaseLLtypeTests):
   # for the individual tests see
   # ====> ../../../metainterp/test/test_ajit.py

   def test_free_object(self):
       py.test.skip("issue of freeing, probably with ll2ctypes")

# class TestBasicx86(Jit386Mixin, test_ajit.BaseLLtypeTests):
#     # for the individual tests see
#     # ====> ../../../metainterp/test/test_ajit.py

#     def test_free_object(self):
#         py.test.skip("issue of freeing, probably with ll2ctypes")
