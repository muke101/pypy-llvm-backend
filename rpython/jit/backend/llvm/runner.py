from rpython.jit.backend.llsupport.llmodel import AbstractLLCPU, jitframe
from rpython.jit.backend.model import CPUTotalTracker
from rpython.rtyper.lltypesystem import rffi, lltype, llmemory
from rpython.jit.backend.llvm.llvm_api import LLVMAPI, CString
from rpython.jit.backend.llvm.llvm_parse_ops import LLVMOpDispatcher
from rpython.jit.backend.llvm.assembler import LLVMAssembler
from rpython.rtyper.lltypesystem.rffi import constcharp2str
from rpython.jit.metainterp import history, compile
from rpython.jit.metainterp.support import ptr2int
from rpython.rtyper.annlowlevel import llhelper
import os

class LLVM_CPU(AbstractLLCPU):
    def __init__(self, rtyper, stats, opts=None,
                 translate_support_code=False, gcdescr=None, debug=True):
        AbstractLLCPU.__init__(self, rtyper, stats, opts,
                               translate_support_code, gcdescr)
        self.is_llvm = True
        self.supports_floats = True
        self.supports_singlefloats = False #can't handle how pypy lowers C functions with f32 as ret type
        self.tracker = CPUTotalTracker()
        self.debug = debug
        self.llvm = LLVMAPI()
        self.assembler = LLVMAssembler(self)
        self.thread_safe_context = self.llvm.CreateThreadSafeContext(None)
        self.context = self.llvm.GetContext(self.thread_safe_context)
        self.dispatchers = {} #map loop tokens to their dispatcher instance
        self.descr_tokens = {} #map descrs to token values that llvm uses
        self.force_tokens = {}
        self.assembler_call_token_cnt = 0
        self.assembler_call_tokens = {} #map assembler call token values to looptokens
        self.defined_functions = set() #set of non-trace functions that have already been compiled
        self.descr_token_cnt = 1 #start at 1 because memory is initailised to 0
        self.WORD = 8
        self.define_types()
        self.define_constants()

    def define_types(self):
        self.llvm_bool_type = self.llvm.IntType(self.context, 1)
        self.llvm_char_type = self.llvm.IntType(self.context, self.WORD)
        self.llvm_short_type = self.llvm.IntType(self.context, self.WORD*2)
        self.llvm_24bit_int_type = self.llvm.IntType(self.context, self.WORD*3)
        self.llvm_32bit_int_type = self.llvm.IntType(self.context, self.WORD*4)
        self.llvm_40_int_type = self.llvm.IntType(self.context, self.WORD*5)
        self.llvm_48bit_int_type = self.llvm.IntType(self.context, self.WORD*6)
        self.llvm_56bit_int_type = self.llvm.IntType(self.context, self.WORD*7)
        self.llvm_int_type = self.llvm.IntType(self.context, self.WORD*8)
        self.llvm_wide_int = self.llvm.IntType(self.context, self.WORD*16) #for overflow checks
        self.llvm_float_type = self.llvm.FloatType(self.context) #DoubleTypeInContext
        self.llvm_single_float_type = self.llvm.SingleFloatType(self.context)
        self.llvm_indx_type = self.llvm_32bit_int_type #llvm only allows signed 32bit ints for indecies (for some reason, and only sometimes)
        self.llvm_int_ptr = self.llvm.PointerType(self.llvm_int_type, 0)
        self.llvm_32int_ptr = self.llvm.PointerType(self.llvm_indx_type, 0)
        self.llvm_void_type = self.llvm.VoidType(self.context)
        self.llvm_void_ptr = self.llvm.PointerType(self.llvm.IntType(self.context, 8), 0) #llvm doesn't have void*, represents as i8*
        self.jitframe_type, self.jitframe_subtypes = self.decl_jitframe()
        #defining memory access behaviour constants here too. see https://www.llvm.org/doxygen/classllvm_1_1MemoryEffectsBase.html for descriptions
        self.mem_acc_none = 0
        self.mem_acc_read_only = 1
        self.mem_acc_write_only = 2
        self.mem_acc_argmem_only = 3
        self.mem_acc_inaccessible_only = 4
        self.mem_acc_inaccessible_or_argmem_only = 5
        self.mem_acc_inaccessible_read_only = 6

    def decl_jitframe(self):
        #array length 0 = unknown length
        #though we do currently know the length, it is subject to change as new bridges are added
        #that potentially have more failargs than the input args we started with
        #we could go back through the IR and patch this for a theoretical optimisation improvement
        #likely via TBAA, possibly through vectorisation, but loading from and storing to
        #the jitframe are never in tight loop code, so this is a low priority feature.
        #note that we do still know this value by the time we reach compilation of the IR
        #as we can compile machine code for specific trace trees without having to accomodate
        #future growth, as it will all get recompiled again if a new bridge is attached
        arg_array = self.llvm.ArrayType(self.llvm_int_type, 0)
        jitframe_subtypes = [self.llvm_void_ptr, self.llvm_int_type,
                               self.llvm_int_type, self.llvm_void_ptr,
                               self.llvm_void_ptr, self.llvm_void_ptr,
                               self.llvm_void_ptr, arg_array]
        elem_array = rffi.CArray(self.llvm.TypeRef)
        elem_count = 8
        packed = 0
        elem_types = lltype.malloc(elem_array, n=elem_count, flavor='raw')
        for c, typ in enumerate(jitframe_subtypes):
            elem_types[c] = typ
        jitframe_type = self.llvm.StructType(self.context, elem_types,
                                             elem_count, packed)
        lltype.free(elem_types, flavor='raw')

        return (jitframe_type, jitframe_subtypes)

    def declare_function(self, param_types, ret_type, name, module, variadic=False):
        #takes llvm types
        parameters = LLVMOpDispatcher.rpython_array(param_types, self.llvm.TypeRef)
        signature = self.llvm.FunctionType(ret_type, parameters,
                                           len(param_types),
                                           1 if variadic else 0)
        lltype.free(parameters, flavor='raw')
        cstring = CString(name)
        func = self.llvm.GetNamedFunction(module, cstring.ptr)
        if func._cast_to_int() != 0:
            return (func, signature)
        return (self.llvm.AddFunction(module, cstring.ptr, signature), signature)

    def set_attributes(self, func, attributes, param_attributes=None):
        cpu_name = self.assembler.cpu_name
        cpu_features = self.assembler.cpu_features
        cstring = CString("target-cpu")
        self.llvm.add_function_string_attribute(func, cstring.ptr, cpu_name,
                                                self.context)
        cstring = CString("target-features")
        self.llvm.add_function_string_attribute(func, cstring.ptr, cpu_features,
                                                self.context)

        for attr in attributes:
            cstring = CString(attr)
            self.llvm.add_function_attribute(func, cstring.ptr, cstring.len,
                                             self.context)

        if param_attributes is not None:
            for attr, index in param_attributes:
                cstring = CString(attr)
                self.llvm.add_param_attribute(func, cstring.ptr, cstring.len,
                                              self.context, index)

    def set_call_attributes(self, call, attributes=None, param_attributes=None,
                            ret_attributes=None):
        if attributes is not None:
            for attr in attributes:
                cstring = CString(attr)
                kind = self.llvm.GetAttributeKindForName(cstring.ptr,
                                                         cstring.len)
                attribute = self.llvm.CreateEnumAttribute(self.context,
                                                          kind, 0)
                self.llvm.AddCallSiteAttribute(call, -1, attribute)

        if param_attributes is not None:
            for attr, index in param_attributes:
                cstring = CString(attr)
                kind = self.llvm.GetAttributeKindForName(cstring.ptr,
                                                         cstring.len)
                attribute = self.llvm.CreateEnumAttribute(self.context,
                                                          kind, 0)
                # note that param indecies are 1-indexed
                self.llvm.AddCallSiteAttribute(call, index, attribute)

        if ret_attributes is not None:
            for attr in ret_attributes:
                cstring = CString(attr)
                kind = self.llvm.GetAttributeKindForName(cstring.ptr,
                                                         cstring.len)
                attribute = self.llvm.CreateEnumAttribute(self.context,
                                                          kind, 0)
                self.llvm.AddCallSiteAttribute(call, 0, attribute)

    def define_constants(self):
        self.zero = self.llvm.ConstInt(self.llvm_int_type, 0, 1)
        self.true = self.llvm.ConstInt(self.llvm_bool_type, 1, 0)
        self.false = self.llvm.ConstInt(self.llvm_bool_type, 0, 0)
        cstring = CString("prof")
        self.prof_kind_id = self.llvm.GetMDKindID(self.context,
                                                  cstring.ptr, cstring.len)
        cstring = CString("dereferenceable")
        self.deref_kind_id = self.llvm.GetMDKindID(self.context,
                                                   cstring.ptr, cstring.len)
        cstring = CString("guard_mark")
        self.guard_kind_id = self.llvm.GetMDKindID(self.context,
                                                   cstring.ptr, cstring.len)
        cstring = CString("bailout_mark")
        self.bailout_kind_id = self.llvm.GetMDKindID(self.context,
                                                      cstring.ptr, cstring.len)
        self.max_int = self.llvm.ConstInt(self.llvm_wide_int,
                                          (1 << (self.WORD*8-1))-1, 1)
        self.min_int = self.llvm.ConstInt(self.llvm_wide_int,
                                          -(1 << (self.WORD*8-1)), 1)

    def verify(self, module):
        verified = self.llvm.VerifyModule(module)
        if verified: #returns 0 on success
            raise Exception("Malformed IR")

    def write_ir(self, module, name):
        cstring = CString("IR/ir-"+name+".bc")
        self.llvm.WriteBitcodeToFile(module, cstring.ptr)
        os.system("llvm-dis IR/ir-"+name+".bc")
        os.system("rm IR/ir-"+name+".bc")

    def dump_looptoken(self, looptoken): #only dumps unoptimised IR
        dispatcher = self.dispatchers[looptoken]
        module = dispatcher.module
        self.write_ir(module, "dmp")
        os.system("cat IR/ir-dmp.ll")
        os.system("rm IR/ir-dmp.ll")

    def set_debug(self, value):
        previous_value = self.debug
        self.debug = value
        return previous_value

    def force(self, force_token):
        deadframe_addr = self.read_int_at_mem(force_token, 0, self.WORD, 0)
        deadframe = rffi.cast(jitframe.JITFRAMEPTR, deadframe_addr)
        deadframe = deadframe.resolve()
        force_descr_token = rffi.cast(lltype.Signed, deadframe.jf_force_descr)
        num_failargs = self.force_tokens[force_descr_token]
        deadframe.jf_descr = rffi.cast(llmemory.GCREF, force_descr_token)
        descr = self.gc_ll_descr.getframedescrs(self).arraydescr
        ofs = self.unpack_arraydescr(descr)
        for i in range(num_failargs):
            failarg = self.read_int_at_mem(force_token, self.WORD*(i+1),
                                           self.WORD, 0)
            self.write_int_at_mem(deadframe, ofs+(self.WORD*i), self.WORD,
                                  failarg)

        return deadframe

    def record_assembler_call(self, token, looptoken):
        self.assembler_call_tokens[token] = looptoken

    def get_assembler_call_addr(self, token):
        looptoken = self.assembler_call_tokens[token]
        return looptoken._ll_function_addr

    def redirect_call_assembler(self, oldlooptoken, newlooptoken):
        for token in self.assembler_call_tokens:
            if self.assembler_call_tokens[token] == oldlooptoken:
                self.assembler_call_tokens[token] = newlooptoken

    def invalidate_loop(self, looptoken):
        dispatcher = self.dispatchers[looptoken]
        rffi.c_memset(rffi.cast(rffi.VOIDP, dispatcher.invalidated_array), 1,
                      dispatcher.invalidated_indx)

    def free_loop_and_bridges(self, compiled_loop_token):
        #FIXME
        func = compiled_loop_token.looptoken.func
        self.llvm.DeleteFunction(func)
        #TODO: track and update asmmemmgr.total_mallocs

    def malloc_gc_wrapper(self, size):
        # llexternal functions don't play nice with LLVM
        return self.gc_ll_descr.malloc_fn_ptr(size)

    def compile_loop(self, inputargs, operations, looptoken, jd_id=0,
                     unique_id=0, log=True, name='trace', logger=None):
        name += str(self.tracker.total_compiled_loops)
        if looptoken in self.dispatchers:
            print("looptoken already compiled:", name)
            return
        cstring = CString(name)
        module = self.llvm.CreateModule(cstring.ptr, self.context)
        builder = self.llvm.CreateBuilder(self.context)
        jitframe_ptr = self.llvm.PointerType(self.jitframe_type, 0)
        trace, _ = self.declare_function([jitframe_ptr, self.llvm_void_ptr],
                                         jitframe_ptr, name, module)
        attributes = ["willreturn", "nounwind", "norecurse"]
        attributes = ["hot", "willreturn", "nounwind", "nofree", "nosync"]
        #FIXME: shouldn't this be index 2?
        param_attributes = [("noalias", 1), ("nocapture", 1)]
        self.set_attributes(trace, attributes, param_attributes)
        cstring = CString("entry")
        entry = self.llvm.AppendBasicBlock(self.context, trace,
                                           cstring.ptr)
        self.llvm.SetModuleDataLayout(module, self.assembler.data_layout)
        self.llvm.SetTarget(module, self.assembler.triple)
        dispatcher = LLVMOpDispatcher(self, builder, module,
                                      entry, trace, looptoken)
        dispatcher.jitframe_depth = len(inputargs)
        self.dispatchers[looptoken] = dispatcher
        dispatcher.dispatch_ops(inputargs, operations)
        if self.debug:
            self.verify(module)
            self.write_ir(module, "org")
        fail_descr_rd_locs = [rffi.cast(rffi.USHORT, i)
                              for i in range(dispatcher.jitframe_depth)]
        history.BasicFailDescr.rd_locs = fail_descr_rd_locs
        compile.ResumeGuardDescr.rd_locs = fail_descr_rd_locs
        compile.ResumeGuardCopiedDescr.rd_locs = fail_descr_rd_locs
        looptoken.name = name
        looptoken.func = dispatcher.func
        looptoken.jitframe_depth = dispatcher.jitframe_depth
        if self.debug:
            print("loop: ")
            for op in operations:
                print(op)
        self.assembler.jit_compile(module, looptoken, inputargs, dispatcher,
                                   name)

    def compile_bridge(self, faildescr, inputargs, operations, looptoken,
                       jd_id=0, unique_id=0, log=True, name='trace', logger=None):
        dispatcher = self.dispatchers[looptoken]
        dispatcher.dispatch_ops(inputargs, operations, faildescr=faildescr)
        looptoken.jitframe_depth = dispatcher.jitframe_depth
        if self.debug:
            self.verify(dispatcher.module)
            self.write_ir(dispatcher.module, "org")
        fail_descr_rd_locs = [rffi.cast(rffi.USHORT, i)
                              for i in range(dispatcher.jitframe_depth)]
        history.BasicFailDescr.rd_locs = fail_descr_rd_locs
        compile.ResumeGuardDescr.rd_locs = fail_descr_rd_locs
        compile.ResumeGuardCopiedDescr.rd_locs = fail_descr_rd_locs
        name = looptoken.name
        if self.debug:
            print("bridge: "+str(self.tracker.total_compiled_bridges))
            for op in operations:
                print(op)
        self.assembler.jit_compile(dispatcher.module, looptoken,
                                   inputargs, dispatcher,
                                   name, is_bridge=True)

    def parse_arg_types(self, *ARGS):
        types = []
        for arg in ARGS:
            if type(arg) == int:
                types.append(lltype.Signed)
            elif type(arg) == float:
                types.append(lltype.Float)
            elif type(arg) == lltype._ptr:
                types.append(arg._TYPE)
            elif type(arg) == llmemory.AddressAsInt:
                types.append(arg.lltype())
            else:
                raise Exception("Unknown type: ", type(arg))
        return types

    def execute_token(self, looptoken, *ARGS):
        arg_types = self.parse_arg_types(*ARGS)
        func = self.make_execute_token(*arg_types)
        deadframe = func(looptoken, *ARGS)
        return deadframe

    def get_latest_descr(self, deadframe):
        deadframe = lltype.cast_opaque_ptr(jitframe.JITFRAMEPTR, deadframe)
        descr_token = rffi.cast(lltype.Signed, deadframe.jf_descr)
        return self.descr_tokens[descr_token]
