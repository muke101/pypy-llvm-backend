#include "wrapper_cpp.h"
#include <llvm/IR/Instruction.h>
#include <llvm/IR/InstrTypes.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/ADT/APInt.h>
#include <llvm/Support/raw_ostream.h>
#include "llvm/IR/DerivedTypes.h"
#include "llvm/Transforms/Utils/LoopSimplify.h"
#include <llvm/IR/PassManager.h>
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/LoopDistribute.h"
#include "llvm/Transforms/Scalar/LoopInterchange.h"
#include "llvm/Transforms/IPO.h"
#include "llvm-c/Initialization.h"
#include "llvm-c/Transforms/Scalar.h"
#include "llvm-c/TargetMachine.h"
#include "llvm/Analysis/BasicAliasAnalysis.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Analysis/ScopedNoAliasAA.h"
#include "llvm/Analysis/TypeBasedAliasAnalysis.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Verifier.h"
#include "llvm/InitializePasses.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include "llvm/Transforms/Scalar/Scalarizer.h"
#include "llvm/Transforms/Scalar/SimpleLoopUnswitch.h"
#include "llvm/Transforms/Utils/UnifyFunctionExitNodes.h"
#include "llvm/Transforms/IPO/FunctionAttrs.h"
#include "llvm/Transforms/IPO/HotColdSplitting.h"
#include <llvm/ADT/Triple.h>
#include <llvm/Analysis/TargetLibraryInfo.h>
#include <llvm/Analysis/TargetTransformInfo.h>
#include <llvm/IR/DataLayout.h>
#include <llvm/Support/ModRef.h>
//#include <llvm/Support/TargetRegistry.h>
#include <llvm/Target/TargetMachine.h>
#include <cstddef>
#include <sys/types.h>
#include <stdio.h>
using namespace llvm;
using namespace std;

#ifdef __cplusplus
extern "C"{
#endif

    void set_tail_call_wrapper(LLVMValueRef call_inst){
        CallInst *Call = reinterpret_cast<CallInst *>(call_inst);
        Call->setTailCall(true);
        Call->setTailCallKind(llvm::CallInst::TCK_MustTail);
    }

    void set_func_mem_access_wrapper(LLVMValueRef func, unsigned mem_acc){
        MemoryEffects ME = MemoryEffects::unknown();
        switch (mem_acc){
           case 0:
			ME = MemoryEffects::none();
			break;
           case 1:
			ME = MemoryEffects::readOnly();
			break;
           case 2:
			ME = MemoryEffects::writeOnly();
			break;
           case 3:
			ME = MemoryEffects::argMemOnly();
			break;
           case 4:
			ME = MemoryEffects::inaccessibleMemOnly();
			break;
           case 5:
			ME = MemoryEffects::inaccessibleOrArgMemOnly();
			break;
           case 6:
			ME = MemoryEffects::inaccessibleMemOnly();
            ME |= MemoryEffects::readOnly();
			break;
        }
        Function *F = reinterpret_cast<Function *>(func);
        F->setMemoryEffects(ME);
    }

    LLVMTypeRef getIndexedType_wrapper(LLVMTypeRef source_type, LLVMValueRef *indxs, unsigned num_indxs){
        std::vector<Value *> indxs_cast;
        for (unsigned i=0; i < num_indxs; i++)
            indxs_cast.push_back(reinterpret_cast<Value *>(indxs[i]));
        ArrayRef<Value *> indxs_array = ArrayRef<Value *>(indxs_cast);
        Type *source_type_cast = reinterpret_cast<Type *>(source_type);
        return reinterpret_cast<LLVMTypeRef>(GetElementPtrInst::getIndexedType(source_type_cast, indxs_array));
    }

    LLVMValueRef removeIncomingValue_wrapper(LLVMValueRef phi, LLVMBasicBlockRef block){
        PHINode *phi_node = reinterpret_cast<PHINode *>(phi);
        BasicBlock *basic_block = reinterpret_cast<BasicBlock *>(block);
        return reinterpret_cast<LLVMValueRef>(phi_node->removeIncomingValue(basic_block));
    }

    void removePredecessor_wrapper(LLVMBasicBlockRef current, LLVMBasicBlockRef pred){
        BasicBlock *current_block = reinterpret_cast<BasicBlock *>(current);
        BasicBlock *pred_block = reinterpret_cast<BasicBlock *>(pred);
        current_block->removePredecessor(pred_block, true);
    }

    LLVMValueRef getFirstNonPhi_wrapper(LLVMBasicBlockRef block){
        BasicBlock *basic_block = reinterpret_cast<BasicBlock *>(block);
        return reinterpret_cast<LLVMValueRef>(basic_block->getFirstNonPHI());
    }

    LLVMBasicBlockRef splitBasicBlockAtPhi_wrapper(LLVMBasicBlockRef block){
        BasicBlock *basic_block = reinterpret_cast<BasicBlock *>(block);
        Instruction *I = basic_block->getFirstNonPHI();
        return reinterpret_cast<LLVMBasicBlockRef>(basic_block->splitBasicBlock(I));
    }

    LLVMValueRef getTerminator_wrapper(LLVMBasicBlockRef block){
        BasicBlock *basic_block = reinterpret_cast<BasicBlock *>(block);
        return reinterpret_cast<LLVMValueRef>(basic_block->getTerminator());
    }

    void dumpModule_wrapper(LLVMModuleRef mod){
        std::string str;
        llvm::raw_ostream &output = llvm::errs();
        unwrap(mod)->print(output, NULL);
    }

    void dumpBasicBlock_wrapper(LLVMBasicBlockRef block){
        std::string str;
        llvm::raw_ostream &output = llvm::errs();
        unwrap(block)->print(output, NULL);
    }

    LLVMValueRef getIncomingValueForBlock_wrapper(LLVMValueRef phi, LLVMBasicBlockRef block){
        BasicBlock *basic_block = unwrap(block);
        PHINode *phi_node = cast<PHINode>(unwrap(phi));
        return wrap(phi_node->getIncomingValueForBlock(basic_block));
    }

    void AddLoopInterchangePass_wrapper(LLVMPassManagerRef pass_manager){
        unwrap(pass_manager)->add(llvm::createLoopInterchangePass());
    }

    void AddLoopDistributePass_wrapper(LLVMPassManagerRef pass_manager){
        unwrap(pass_manager)->add(llvm::createLoopDistributePass());
    }

    void AddHotColdSplittingPass_wrapper(LLVMPassManagerRef pass_manager){
        unwrap(pass_manager)->add(llvm::createHotColdSplittingPass());
    }

    void AddLoopSimplifyPass_wrapper(LLVMPassManagerRef pass_manager){
        unwrap(pass_manager)->add(llvm::createLoopInstSimplifyPass());
        unwrap(pass_manager)->add(llvm::createLoopSimplifyCFGPass());
    }

    void AddLoopStrengthReducePass_wrapper(LLVMPassManagerRef pass_manager){
        unwrap(pass_manager)->add(llvm::createLoopStrengthReducePass());
        unwrap(pass_manager)->add(llvm::createGVNHoistPass());
    }

    void AddInferFunctionAttrsPass_wrapper(LLVMPassManagerRef pass_manager){
        unwrap(pass_manager)->add(llvm::createPostOrderFunctionAttrsLegacyPass());
    }

    void AddTargetLibraryInfoPass_wrapper(LLVMPassManagerRef pass_manager, char* triple){
        unwrap(pass_manager)->add(new TargetLibraryInfoWrapperPass(Triple(triple)));
    }

    void AddTargetTransformationInfoPass_wrapper(LLVMPassManagerRef pass_manager, LLVMTargetMachineRef target_machine){
        TargetMachine *TM = reinterpret_cast<TargetMachine *>(target_machine);
        unwrap(pass_manager)->add(createTargetTransformInfoWrapperPass(TM->getTargetIRAnalysis()));
    }

    void add_deref_ret_attr(LLVMValueRef call_inst, u_int64_t bytes){
        CallBase *call = reinterpret_cast<CallBase *>(call_inst);
        call->addDereferenceableRetAttr(bytes);
    }

#ifdef __cplusplus
}
#endif
