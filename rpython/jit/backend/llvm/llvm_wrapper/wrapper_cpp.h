#include <llvm-c/Core.h>
#include <llvm-c/TargetMachine.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C"{
#endif
    void set_tail_call_wrapper(LLVMValueRef call_inst);

    void set_func_mem_access_wrapper(LLVMValueRef func, unsigned mem_acc);

    LLVMTypeRef getIndexedType_wrapper(LLVMTypeRef source_type, LLVMValueRef *indxs, unsigned num_indxs);

    LLVMValueRef removeIncomingValue_wrapper(LLVMValueRef phi, LLVMBasicBlockRef block);

    void removePredecessor_wrapper(LLVMBasicBlockRef current, LLVMBasicBlockRef pred);

    LLVMValueRef getFirstNonPhi_wrapper(LLVMBasicBlockRef block);

    LLVMBasicBlockRef splitBasicBlockAtPhi_wrapper(LLVMBasicBlockRef block);

    LLVMValueRef getTerminator_wrapper(LLVMBasicBlockRef block);

    void dumpModule_wrapper(LLVMModuleRef mod);

    void dumpBasicBlock_wrapper(LLVMBasicBlockRef block);

    LLVMValueRef getIncomingValueForBlock_wrapper(LLVMValueRef phi, LLVMBasicBlockRef block);

    void AddInferFunctionAttrsPass_wrapper(LLVMPassManagerRef pass_manager);

    void AddLoopSimplifyPass_wrapper(LLVMPassManagerRef pass_manager);

    void AddLoopStrengthReducePass_wrapper(LLVMPassManagerRef pass_manager);

    void AddTargetLibraryInfoPass_wrapper(LLVMPassManagerRef pass_manager, char* triple);

    void AddTargetTransformationInfoPass_wrapper(LLVMPassManagerRef pass_manager, LLVMTargetMachineRef target_machine);

    void add_deref_ret_attr(LLVMValueRef func, u_int64_t bytes);

    void AddLoopInterchangePass_wrapper(LLVMPassManagerRef pass_manager);

    void AddLoopDistributePass_wrapper(LLVMPassManagerRef pass_manager);

    void AddHotColdSplittingPass_wrapper(LLVMPassManagerRef pass_manager);
#ifdef __cplusplus
}
#endif
